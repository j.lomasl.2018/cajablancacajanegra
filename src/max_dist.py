class InvalidArgument(Exception):
    """Raised when the function is called with invalid arguments."""
    pass

class TypeError(Exception):
    """Raised when the function is called with invalid arguments."""
    pass
        
def max_dist(n=None):
    if type(n) != list or len(n) < 2:
        raise InvalidArgument
    for element in n:
        if not isinstance(element, int):
            raise TypeError

    max_difference = 0  
    for i in range(0, len(n) - 1):
        diff = abs(n[i] - n[i + 1])
        if diff > max_difference:
            max_difference = diff

    return max_difference
