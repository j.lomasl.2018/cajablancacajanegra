def dos_menores(a=None):
    if type(a) != list or len(a) < 2:
        return None

    min1 = min(a[0], a[1])
    min2 = max(a[0], a[1])

    for i in range(2, len(a)):
        e = a[i]
        if e < min1:
            min2 = min1
            min1 = e
        elif e < min2:
            min2 = e

    return min1, min2




