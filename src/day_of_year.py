class InvalidArgument(Exception):
    """Raised when the function is called with invalid arguments."""
    pass

def day_of_year(day, month, year):
    if not all(isinstance(arg, int) for arg in (day, month, year)):
        raise InvalidArgument

    if month < 1 or month > 12 or day < 1 or day > 31:
        raise InvalidArgument

    if month == 2 and (day > 29 or (day > 28 and not (year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)))):
        raise InvalidArgument

    days_in_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if month > 1:
        days = sum(days_in_month[1:month]) + day
    else:
        days = day

    if month > 2 and (year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)):
        days += 1

    return days


