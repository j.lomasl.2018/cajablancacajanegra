import pytest
from day_of_year import day_of_year, InvalidArgument

@pytest.mark.parametrize("day, month, year, expected_output, exception_raised", [
    (1, 1, 1987, 1, None),
    (1, 2, 1988, 32, None),
    (1, 3, 1984, 61, None),
    (1, 3, 1985, 60, None),
    (22, 13, 1970, None, InvalidArgument),
    (22, 1970, None, None, InvalidArgument),
    (None, None, None, None, InvalidArgument),
    ("1", 1, 1970, None, InvalidArgument),
])
def test_day_of_year(day, month, year, expected_output, exception_raised):
    if exception_raised:
        with pytest.raises(exception_raised):
            day_of_year(day, month, year)
    else:
        result = day_of_year(day, month, year)
        assert result == expected_output

