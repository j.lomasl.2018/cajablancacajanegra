import pytest
from dos_menores import dos_menores 

@pytest.mark.parametrize("input_list, expected_output", [
    ([3, 1, 4, 1], (1, 1)),
    ([5, 2, 8, 7], (2, 5)),
    ([9, 9, 9], (9, 9)),
    ([2, 4, 1, 5], (1, 2)),
    ([], None),
    ("not a list", None),
    (42, None),
])
def test_dos_menores(input_list, expected_output):
    result = dos_menores(input_list)
    assert result == expected_output
