import pytest
from max_dist import max_dist, InvalidArgument, TypeError

@pytest.mark.parametrize("input_list, expected_output, exception_raised", [
    ([8, 2, -3], 6, None),
    ([5, 2, 8, 7], 6, None),
    ([1, 1, 1, 1], 0, None),
    ([], None, InvalidArgument),
    ([4], None, InvalidArgument),
    (["a", 2, 3], None, TypeError),
    ([1, "b", 3], None, TypeError),
    ([1, 2, "c"], None, TypeError),
])
def test_max_dist(input_list, expected_output, exception_raised):
    if exception_raised:
        with pytest.raises(exception_raised):
            max_dist(input_list)
    else:
        result = max_dist(input_list)
        assert result == expected_output
