import pytest
from g import g

def test_g_for_even_number():
    assert g(2) == 1

def test_g_for_odd_number():
    assert g(3) == 1

def test_g_for_large_number():
    assert g(1000) == 1
